<?php

namespace Src\models;


class BookingModel {

	private $bookingData;

	function __construct() {
		$string = file_get_contents(dirname(__DIR__) . '/../scripts/bookings.json');
		$this->bookingData = json_decode($string, true);
	}


	public function createBooking($client_id, $price, $checkIn, $checkOut)
	{
		$checkInFormatted = gmdate("Y-m-d H:i:s", strtotime($checkIn));
		$checkOutFormatted = gmdate("Y-m-d H:i:s", strtotime($checkOut));
		$this->bookingData[] = [
			"id" => end($this->bookingData)['id'] + 1,
			"clientid" => $client_id,
			"price" => $price,
			"checkindate" => $checkInFormatted,
			"checkoutdate" => $checkOutFormatted
		];
		$this->helper->putJson($this->bookingData, 'bookings');
	}

	public function getBookings() {
		return $this->bookingData;
	}
}
